/*
 * menuExpandable2.js - implements an expandable menu based on a HTML list
 * Author: Dave Lindquist (dave@gazingus.org)
 */

if (!document.getElementById)
    document.getElementById = function() { return null; }

function deliciousInitializeMenu(menuId, actuatorId) {
    var menu = document.getElementById(menuId);
    var actuator = document.getElementById(actuatorId);

    if (menu == null || actuator == null) return;

    // if (window.opera) return; // Opera 7 works well, so commenting this

    actuator.parentNode.style.backgroundImage = "url(modules/delicious/plus.gif)";
    actuator.onclick = function() {
        var display = menu.style.display;
        this.parentNode.style.backgroundImage =
            (display == "block") ? "url(modules/delicious/plus.gif)" : "url(modules/delicious/minus.gif)";
        menu.style.display = (display == "block") ? "none" : "block";

        return false;
    }
}



