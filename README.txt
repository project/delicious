
del.icio.us module
0.6
Originally By: benjamin grant (bgrant@1361b.net)
Maintained By: Earl Miles (merlin@logrus.com)

August 8, 2005

DESCRIPTION
  * enables the creation of an entry at del.icio.us with each blog posting
  * provides a recently added block for your del.icio.us bookmarks
  * provides "smart tagging" of node body and teaser contents based on
    del.icio.us user tags (code ported from j. deutsch's wordpress plugin)
  * test site: http://drupal.1361b.net/
  * allows each user to have own del.icio.us links
  * allows customized blocks based upon combinations of users and tags


INSTRUCTIONS:

1) install this to your modules directory

2) install delicious.mysql using mysql -u user -p database < delicious.mysql

3) enable it at administer->modules

4) configure it at administer->settings->delicious

5) configure del.icio.us blocks at administer->delicious blocks

6) enable a delicious username & password at my account->delicious


NOTES

The link and tag lists are updated by drupal's cron service.

Because del.icio.us requests that updates not be done more than every 30 
minutes, there is no provision to force an update other than by hitting 
cron.php. Hence there is a lag between the time you add a link
and it appears in the sidebar block this module generates.

"smart tags" are inserted dynamically, not saved in the node content, so
old nodes will be marked up with the latest tags.

Earl notes:
  * I don't know anything about postgres, so  I didn't create a .pgsql file. 
    I believe I used the appropriate SQL to ensure database independence, but
    I cannot verify that. If anybody wants to test this module under postgres
    and report, that would be appreciated
  * I ripped off the actuator stuff from taxonomy_dhtml because I like it. I'm
    not sure how well that'll hold up if someone has massive numbers of links.
    This needs further testing; if that doesn't work, I may have to do away with
    that piece of code entirely and use a simpler query.

SEE ALSO

  waypath.module for related links

TODO

    * let _block configure also add form items?
    * Is there a way to get the block config to work under administer/blocks
      and still make sense?
    * write appropriate help bits
    * document functions, and doxygen theme_ functions
    * double check to make sure everything is to coding standard.
    * postgresSQL database

USEFUL?

  do you find this module useful?  please consider making a small
  donation to the children of san juan del sur, nicaragua.  more
  information can be found at:

    http://www.sanjuandelsur.org.ni/community/brugger/index.htm

CHANGELOG

10/27/2005
  Added 2 fields to the database. Because the taxonomy DHTML stuff is slow with lots of links, individual users can now choose which display to use. Thanks to willwade for the help.

  Fixed problems with db_rewrite_sql.

  Fixed some general UI errors and a some bad SQL. IF UPGRADING YOU MUST RERUN DATABASE.MYSQL!!!!!

